#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y
#instalo o docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
#instalo o gitlab runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
#jogando o usuário gitlab-runner no grupo docker, usando o usermod
#o usermod realiza alterações no usuário
#o -aG diz o grupo(docker) e o usuário(gitlab-runner)
sudo usermod -aG docker gitlab-runner
sudo gitlab-runner register \
--non-interactive \
--url https://gitlab.com \
--token glrt-vvuT9vsBC4bsi6KM4aSn \
--executor shell \